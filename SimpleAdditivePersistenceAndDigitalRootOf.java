// Project Challenge: https://rosettacode.org/wiki/Digital_root

import java.math.BigInteger;

public class SimpleAdditivePersistenceAndDigitalRootOf {

    public static void main(String[] args) {
        String[] N = getValidNumber(args);

        String[] X = calculateAdditivePersistenceAndDigitalRootOf(N);

        System.out.printf(
            "%s (base %s) has additive persistence %s and digital root of %s\n",
            N[0], N[1], X[0], X[1]
        );
    }

    private static String[] getValidNumber(String[] args) {
        String n = args[0];
        String[] number = new String[2];
        try {
            int base = Integer.parseInt(args[1]);
            if (base <= Character.MAX_RADIX && base >= Character.MIN_RADIX) {
                BigInteger N = new BigInteger(n, base);
                number[0] = N.abs().toString(base);
                number[1] = args[1];
            } else {
                number = new String[]{"0", "10"};
            }
        } catch (Exception e) {
            number = new String[]{"0", "10"};
        }
        return number;
    }

    private static String[] calculateAdditivePersistenceAndDigitalRootOf(String[] N) {
        char[] digits = N[0].toCharArray();
        int base = Integer.parseInt(N[1]);

        int digitalRoot = 0; String X = "";
        long additivePersistence = 0L;

        do {
            for (int i = 0; i < digits.length; i++) digitalRoot += Character.digit(digits[i], base);
            additivePersistence++;

            X = Integer.toString(digitalRoot, base);
            if (X.length() == 1) {
                return new String[]{Long.toString(additivePersistence), X};
            } else {
                digits = X.toCharArray();
                digitalRoot = 0;
            }
        } while (true);
    }
}
