# Digital Root

Description: [https://rosettacode.org/wiki/Digital_root](https://rosettacode.org/wiki/Digital_root)

My implementation takes in the number and its radix as command-line arguments when determining its additive persistence and digital root.

# How to run

## Simple version

```
$ git clone https://radman13666@bitbucket.org/radman13666/digital-root.git
$ cd digital-root
$ javac SimpleAdditivePersistenceAndDigitalRootOf.java
$ # Example in the description (in bases 10 and 16)
$ java SimpleAdditivePersistenceAndDigitalRootOf 627615 10
$ java SimpleAdditivePersistenceAndDigitalRootOf 627615 16
```
